import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'college',
    loadChildren: () => import('./college/college.module').then(m => m.CollegeModule)
  },
  {
    path: '',
    redirectTo: 'college',
    pathMatch:'full'
  } 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
