import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BranchesComponent } from './components/branches/branches.component';
import {StaffComponent } from './components/staff/staff.component';



const routes: Routes = [
  {path:"branches",component:BranchesComponent},
  {path:"staff",component:StaffComponent},
  { path: '', redirectTo: "college", pathMatch: "full" }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollegeRoutingModule { }
